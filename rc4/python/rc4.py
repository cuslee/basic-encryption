# rc4 in python for encrypting
# decrypt does not work yet

def main():
    file_method = input("Would you like to Encrypt or Decrypt File: ")

    S = [None]* 256
    E = [None]* 256
    file_name = input("What is the file you want to Encrypt or Decrypt: ")

    key = input("What is the Key: ")

    print("Opening File...")
    f = open(file_name, "r", encoding="utf-8")

    print("Reading file contents...")
    read_data = f.read()

    keysize = len(key) - 1
    cipher = [None]* len(read_data)
    decrypt = [None]* len(read_data)

    if file_method == "encrypt":
        ksa(S, read_data, keysize)
        prga(S, read_data, cipher, keysize)
        f.close()
        f = open(file_name, "w", encoding="utf-8")
        for i in range(len(read_data)):
            print(cipher[i].encode("utf-8"), end=" ")
            f.write(cipher[i])

    elif file_method == "decrypt":
        ksa(E, read_data, keysize)
        prga(E, cipher, decrypt, keysize)
        f.close()
        f= open(file_name, "w", encoding="utf-8")
        for i in range(len(read_data)):
            print(decrypt[i].encode("utf-8"), end=" ")
            f.write(decrypt[i])

    print()
    print("Closing File...")
    f.close()


def ksa(s,k,length):
    j = temp = 0
    t = [None]* 256
    for i in range(256):
        s[i] = i
        t[i] = k[i%length]

    for i in range(256):
        j = (j + s[i] + ord(t[i])) % 256
        temp = s[i]
        s[i] = s[j]
        s[j] = temp


def prga(s, p, c, keysize):
    i = j = temp = 0
    for x in range(len(p)):
        i = (i + 1) % 256
        j = (j + s[i]) % 256
        temp = s[i]
        s[i] = s[j]
        s[j] = temp
        k = s[(s[i] + s[j]) % 256]
        c[x] = str(chr((ord(p[x])) ^ ord(chr(k))))


if __name__ == '__main__':
    main()
